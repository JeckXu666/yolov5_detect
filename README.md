1. [TOC]

   # 基于yolo_v5实现王者荣耀辅助瞄准

   ## 一、文章前言

   > 本文只用于学习参考，禁止用于违法用途

   玩王者的时候用的百里，有一个瞄准技能，因为经常瞄不准，所以想到使用 yolov5 识别模型，识别目标位置，通过反馈的位置，用脚本瞄准目标：

   ![游戏画面](README/re-16510437387231.gif)

   最终做出的效果如下：

   ![运行结果](README/compare-16510437407312.gif)

   

   本篇文章简单的分享一下实现思路和步骤

   ## 二、工具&环境准备

   ### 2.1 安装必要环境工具

   IDE：使用 Pycharm 2021 社区版，推荐安装参考博文：[pycharm安装教程，超详细](https://blog.csdn.net/qq_44809707/article/details/122501118 "pycharm安装教程，超详细")

   CUDA 和 CUDNN：用于使用 GPU 加速训练和推理，推荐安装参考博文：[Cuda和cuDNN安装教程(超级详细)](https://blog.csdn.net/jhsignal/article/details/111401628 "Cuda和cuDNN安装教程(超级详细)")

   Git：便于进行模型操作和环境配置，推荐安装参考博文：[Git安装教程（Windows安装超详细教程）](https://www.jianshu.com/p/414ccd423efc "Git安装教程（Windows安装超详细教程）")

   Anconada：便于配置管理 Python 环境，推荐安装参考博文：[Win10+Anaconda3 的详细安装教程（图文并茂）](https://blog.csdn.net/scorn_/article/details/106591160 "Win10+Anaconda3 的详细安装教程（图文并茂）")

   AnLink：将手机画面投屏到电脑的工具，下载链接：[AnLink官网](https://anl.ink/?lang=en&uuid={4FB8B6DE-7C41-4117-A963-864773E23C88} "AnLink官网")

   ### 2.2 获取 yolov5 源码                                                             

   源码获取推荐参考文章：[YOLOv5的详细使用教程，以及使用yolov5训练自己的数据集](https://blog.csdn.net/sinat_28371057/article/details/120598220 "YOLOv5的详细使用教程，以及使用yolov5训练自己的数据集")

   步骤简单总结一下就是：

   - 使用 Git 工具拉取源码
   - 使用 Conda 安装配置 yolo 需要的环境

   ## 三、模型训练

   准备一段用于训练的视频，这里我用的小鲁班作为目标

   ![鲁班7号](README/20220427111611-16510437433993.png)

   将这段视频导出为图片，推荐训练的张数为 100 - 400 张，导出可以使用 PR 或者一些网页进行转化，转换网页推荐：[Convert To JPG](https://www.img2go.com/convert-to-jpg "Convert To JPG")

   有了图片后使用 Labelimg 进行标注，在 Git 中使用如下命令下载：

   ```
   pip install labelimg
   ```

   下载后启动：

   ```
   labelimg
   ```

   选择 yolo 标注模式进行标注，设置目标标签为 luban，标注文件生成的是 txt，用于后续的训练

   ![yolo检测](README/20220427113324-16510437450524.png)

   这里的 labelimg 标注工具有一些使用细节要注意，推荐大家参考阅读这篇文章：[labelImg 使用教程 图像标定工具](https://blog.csdn.net/Dontla/article/details/102662815 "labelImg 使用教程 图像标定工具")

   数据集准备好后，我们放到同一个文件夹下

   ![文件目录](README/20220427113618-16510437464845.png)

   用 pytorch 打开代码工程，在如下位置新建一个数据集配置文件

   ![pytorch工程](README/20220427113857-16510437480006.png)

   输入配置参数：

   ```
   train: D:\yolo_v5\datasets\trains2  # train images (relative to 'path') 128 images
   val: D:\yolo_v5\datasets\trains2  # val images (relative to 'path') 128 images
   nc: 1  # number of classes
   names: ['luban']  # class names
   ```

   - train：训练集标签路径
   - val：验证集标签路径（理论上两个数据集应该不一样，训练效果更好，这里我使用了同一个）
   - nc：识别的种类数目
   - names：识别的标签，和标注时设置的标签要对上

   之后修改训练脚本 train.py 参数：

   ![修改训练脚本](README/20220427114359-16510437497177.png)

   配置 pycharm 运行此程序，python 的运行环境是我用 conda 新建的环境

   ![修改程序运行参数](README/20220427114502-16510437513268.png)

   训练现象：GPU 进入打工人状态，整体 200 多张图片训练了 4 个小时左右

   ![运行过程](README/20220427114835-16510437528489.png)

   模型训练完成后测试训练结果：训练模型结果放在了 `yolo_v5\yolov5\runs\train\expxx` 目录下面，模型文件是 best.pt 文件

   ![训练结果](README/20220427121927-165104375464410.png)

   我们修改 detect.py 检测脚本

   ![检推理脚本](README/20220427122105-165104375582611.png)

   然后运行检推理检测程序，运行后在输出目录下生成检测的视频文件，结果如下：

   ![识别结果](README/detect-165104375727412.gif)

   观察效果也可以看训练目录下的参数结果：

   ![loss参数图](README/20220427122619-165104375852413.png)

   整体参数还行，loss 曲线正常拟合，没有过拟合和欠拟合情况

   ## 四、自瞄脚本

   自瞄脚本基于推理代码进行修改的，这里呢我也参考了 CSDN 博主阿尔法羊写的博客：

   [Yolov5自学笔记之二--在游戏中实时推理并应用（实例：哈利波特手游跳舞小游戏中自动按圈圈）](https://blog.csdn.net/qq_41597915/article/details/122738456 "Yolov5自学笔记之二--在游戏中实时推理并应用（实例：哈利波特手游跳舞小游戏中自动按圈圈）")

   下面展示部分源码，具体关键源码可以上 Gitee 仓库下载：[Gitee仓库|可以阅读原文直接获取](https://gitee.com/JeckXu666/yolov5_detect)

   传入模型参数

   ```python
   def detect(
           # --------------------这里更改配置--------------------
           # ---------------------------------------------------
           weights='runs/train/exp40/weights/best.pt',  # 训练好的模型路径
           imgsz=(640,640),  # 训练模型设置的尺寸
           cap=0,  # 摄像头
           conf_thres=0.25,  # 置信度
           iou_thres=0.45,  # NMS IOU 阈值
           max_det=1000,  # 最大侦测的目标数
           device='0',  # 设备
           crop=True,  # 显示预测框
           classes=None,  # 种类
           agnostic_nms=False,  # class-agnostic NMS
           augment=False,  # 是否扩充推理
           half=False,  # 使用FP16半精度推理
           hide_labels=False,  # 是否隐藏标签
           hide_conf=False,  # 是否隐藏置信度
           line_thickness=3  # 预测框的线宽
   ):
   ```

   获取 Anlink 投屏到电脑的游戏画面

   ```python
           image_array = grab_screen(region=(88, 92, 1914, 914))
   
           array_to_image = Image.fromarray(image_array, mode='RGB')
           img = np.asarray(array_to_image)  # 将图像转成array
           # 设置labels--记录标签/概率/位置
           labels = []
           # 计时
           t0 = time.time()
           img0 = img
           # 填充调整大小
           img = letterbox(img0, imgsz, stride=stride)[0]
   
           # Convert
           img = img.transpose((2, 0, 1))[::-1]  # HWC to CHW, BGR to RGB
           img = np.ascontiguousarray(img)
   ```

   模型推理

   ```python
           # 推断
           pred = model(img, augment=augment)[0]
   ```

   推理完成后处理数据

   ```python
   # 获取中心点，即分别求横、纵坐标的中间点
   pointx = int((xyxy[0] + xyxy[2]) / 2)
   pointy = int((xyxy[1] + xyxy[3]) / 2)
   
   # # # 移动鼠标到中心点位置，并点击
   if mark == 0:
       pyautogui.moveTo(1437, 662, _pause=False)
       pyautogui.mouseDown()  # 鼠标按下
       my_ts1 = time.time()
       mark = 1
   else:
       my_ts2 = time.time()
       d2x = pointx - 1826 / 2
       d2y = pointy - 822 / 2
       a = math.atan2(d2y, d2x)
       if 0 < a < pi / 2:
           b = a - (pi / 12) * (1 - math.tan(abs(pi / 4 - a)))
       if pi / 2 <= a < pi:
           b = a - (pi / 12) * (1 - math.tan(abs(pi * 3 / 4 - a)))
       if -pi < a < -pi / 2:
           b = a + (pi / 12) * (1 - math.tan(abs(pi * 3 / 4 + a)))
       if -pi / 2 <= a <= 0:
           b = a - (pi / 12) * (1 - math.tan(abs(pi / 4 + a)))
   
       pyautogui.moveTo(1437 + 140 * math.cos(b), \
                        662 + 140 * math.sin(b), _pause=False)
       if (my_ts2 - my_ts1) > 3:
           pyautogui.mouseUp()  # 鼠标释放
           mark = 0
   ```

   实现原理就是获取目标距离画面中心的位置信息，进入角度转换，因为王者的画面是 2.5D，不是 2D，所以需要做一个坐标转换，就是下面的代码，加一个角度偏置，偏置角度和位置有关，这个转换还是有些小瑕疵，后面需要优化一下，暂时先参考参考，屏幕的触摸使用的是 pyautogui 库

   ```python
       if 0 < a < pi / 2:
           b = a - (pi / 12) * (1 - math.tan(abs(pi / 4 - a)))
       if pi / 2 <= a < pi:
           b = a - (pi / 12) * (1 - math.tan(abs(pi * 3 / 4 - a)))
       if -pi < a < -pi / 2:
           b = a + (pi / 12) * (1 - math.tan(abs(pi * 3 / 4 + a)))
       if -pi / 2 <= a <= 0:
           b = a - (pi / 12) * (1 - math.tan(abs(pi / 4 + a)))
   ```

   最后将识别结果标记画面显示在屏幕上，方便我们直观看到结果

   ```python
   imshow = cv2.cvtColor(im0, cv2.COLOR_BGR2BGRA)
   cv2.imshow("copy window", imshow)
   key = cv2.waitKey(2)
   ```

   ## 五、测试效果

   运行程序，观察现象，最后就是开始的画面了：

   ![运行结果](README/compare-16510409976283-165104376398314.gif)

   ## 六、整体思考

   从模型部署到脚本完成大概花了一天，麻烦的点就是那个 2.5D 到 2D 的坐标转换，花了一些时间，最后出来的自瞄效果还算可以，测试的对象是个人机小鲁班，命中率挺高，我也做过真人测试，测试结果不是很理想，自瞄反应速率跟不上，主要还是处理的帧率太慢，对此可以进行改进：

   - 对模型进行量化，用精度的牺牲换取速度的提升
   - 瞄准脚本可以做优化，加入距离判断，合理的设置技能释放时间
